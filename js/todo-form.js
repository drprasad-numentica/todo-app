const Input = semanticUIReact.Input;
let Button = semanticUIReact.Button;

const TodoForm = ({addTodo}) => {
  let input;

  return (
    <div>
      <Input ref={node => {input = node;}} focus placeholder='Add new ToDo...' />
      <Button onClick={() => {addTodo(input.value); input.value='';}} primary>+</Button>
    </div>
  );
}